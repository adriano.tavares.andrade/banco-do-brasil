
var express = require('express');
var app = express();
const path = require('path');
var mysql = require('mysql');
var myConnection = require('express-myconnection');
const bodyParser = require('body-parser')

app.use(bodyParser.urlencoded({
    extended: true
  }))
  
const Routes = require('./routes/route');

app.use(myConnection(mysql, {
    host: 'localhost',
    user: 'root',
    password: '06142225',
    port: 3306,
    database: 'teste'
  }, 'single'))

app.use('/', Routes)

const PORT = 3000

app.set('views', path.join(__dirname, 'views'));
app.use(express.static(path.join(__dirname, 'public')));
app.set('view engine', 'ejs');

app.set('port', process.env.PORT || 3000);

app.listen(PORT, () => {
    console.log(`server on port ${app.get('port')}`);
  });
