const router = require('express').Router();

const controller = require('../controllers/controller');

router.get('/', controller.login);

router.post('/cadastro', controller.cadastro);

module.exports = router;