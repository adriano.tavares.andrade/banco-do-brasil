function abrir(){
    alert("Olá, seja bem vindo");
}

function fechar(){
    alert("Até a próxima");
}

function validar(){
    if(document.projeto.nome.value==""){
        alert("Campo nome vazio");
        document.projeto.nome.focus();
        return false;
    }

    if(document.projeto.cpf.value==""){
        alert("Campo CPF vazio");
        document.projeto.cpf.focus();
        return false;
    }

    if(document.projeto.matricula.value==""){
        alert("Campo matrícula vazio");
        document.projeto.matricula.focus();
        return false;
    }

    if(document.projeto.dataDeNascimento.value==""){
        alert("Campo data de nascimento vazio");
        document.projeto.dataDeNascimento.focus();
        return false;
    }

    alert("Dados enviados");
}

function limpar(){
    alert("Dados reiniciados");
}

function mudaCor(obj,tipo){
    if(tipo == 1){
        obj.style.backgroundColor="gray";
        obj.style.color="white";
    } else if(tipo == 2) {
        obj.style.backgroundColor="white";
        obj.style.color="black";
    }
}