const controller = {};

controller.login = (req, res) => {
    res.render('projeto', {message: null})
}

controller.cadastro = (req, res) => {
    const nome = req.body.nome;
    const cpf = req.body.cpf;
    const matricula = req.body.matricula;
    const dataDeNascimento = req.body.dataDeNascimento;

    req.getConnection((err, connection) => {
        const query = connection.query('SELECT * FROM teste.funcionarios WHERE Nome= ? ;', nome, (err, result) => {

            const linhas = (Object.keys(result).length);

            if (linhas > 0) {
                res.render('projeto.ejs', {
                    message: 'Usuário ' + nome + ' já existe.'
                })
            }
            else {
                const query1 = connection.query('INSERT INTO teste.funcionarios (Nome, CPF, Matrícula, DataDeNascimento) VALUES (?, ?, ?, ?)', [nome, cpf, matricula, dataDeNascimento], (err, result1) => {
                    res.render('projeto.ejs', {
                        message: 'Usuário cadastrado com sucesso. Favor conferir o e-mail informado.'
                    })
                })
            }
        })
    })
}
module.exports = controller;